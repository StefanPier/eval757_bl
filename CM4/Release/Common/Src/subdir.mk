################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/buffer.c \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/command.c \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/joinedWdt.c \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/manage_debug.c \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.c \
C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/timer.c 

OBJS += \
./Common/Src/buffer.o \
./Common/Src/command.o \
./Common/Src/joinedWdt.o \
./Common/Src/manage_debug.o \
./Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.o \
./Common/Src/timer.o 

C_DEPS += \
./Common/Src/buffer.d \
./Common/Src/command.d \
./Common/Src/joinedWdt.d \
./Common/Src/manage_debug.d \
./Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.d \
./Common/Src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
Common/Src/buffer.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/buffer.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Common/Src/command.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/command.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Common/Src/joinedWdt.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/joinedWdt.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Common/Src/manage_debug.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/manage_debug.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Common/Src/timer.o: C:/Progetti/PortingMain/Esempi/eval757_BL/Common/Src/timer.c Common/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Common-2f-Src

clean-Common-2f-Src:
	-$(RM) ./Common/Src/buffer.d ./Common/Src/buffer.o ./Common/Src/buffer.su ./Common/Src/command.d ./Common/Src/command.o ./Common/Src/command.su ./Common/Src/joinedWdt.d ./Common/Src/joinedWdt.o ./Common/Src/joinedWdt.su ./Common/Src/manage_debug.d ./Common/Src/manage_debug.o ./Common/Src/manage_debug.su ./Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.d ./Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.o ./Common/Src/system_stm32h7xx_dualcore_boot_cm4_cm7.su ./Common/Src/timer.d ./Common/Src/timer.o ./Common/Src/timer.su

.PHONY: clean-Common-2f-Src

