################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/canopen/co_main.c \
../Core/Src/canopen/co_microdep.c \
../Core/Src/canopen/co_nmt.c \
../Core/Src/canopen/co_pdo.c \
../Core/Src/canopen/co_sdo.c 

OBJS += \
./Core/Src/canopen/co_main.o \
./Core/Src/canopen/co_microdep.o \
./Core/Src/canopen/co_nmt.o \
./Core/Src/canopen/co_pdo.o \
./Core/Src/canopen/co_sdo.o 

C_DEPS += \
./Core/Src/canopen/co_main.d \
./Core/Src/canopen/co_microdep.d \
./Core/Src/canopen/co_nmt.d \
./Core/Src/canopen/co_pdo.d \
./Core/Src/canopen/co_sdo.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/canopen/%.o Core/Src/canopen/%.su: ../Core/Src/canopen/%.c Core/Src/canopen/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I"C:/Progetti/PortingMain/Esempi/eval757_BL/CM4/Drivers" -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-canopen

clean-Core-2f-Src-2f-canopen:
	-$(RM) ./Core/Src/canopen/co_main.d ./Core/Src/canopen/co_main.o ./Core/Src/canopen/co_main.su ./Core/Src/canopen/co_microdep.d ./Core/Src/canopen/co_microdep.o ./Core/Src/canopen/co_microdep.su ./Core/Src/canopen/co_nmt.d ./Core/Src/canopen/co_nmt.o ./Core/Src/canopen/co_nmt.su ./Core/Src/canopen/co_pdo.d ./Core/Src/canopen/co_pdo.o ./Core/Src/canopen/co_pdo.su ./Core/Src/canopen/co_sdo.d ./Core/Src/canopen/co_sdo.o ./Core/Src/canopen/co_sdo.su

.PHONY: clean-Core-2f-Src-2f-canopen

