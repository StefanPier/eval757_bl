/*! \file co_sdo.h \brief SDO management module */
//*****************************************************************************
//
// File Name	: 'co_sdo.h'
// Title		: Module containing the Service Data Object management routines.
// Author		: Alessandro Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************

#ifndef CO_SDO_H_
#define CO_SDO_H_

#include "co_main.h"

//!< Public function to send an SDO message to node server
int co_sdo_Send(co_msg_t * msg);

#endif
