/*! \file co_pdo.h \brief CANopen PDO management functions. */
//*****************************************************************************
//
// File Name	: 'co_pdo.h'
// Title		: CANopen PDO management functions.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 06/09/2014
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************
#ifndef PDO_H_
#define PDO_H_

#include "main.h"

//!< Send PDOn to specified node.
void co_pdo_Send(uint32_t n, uint8_t node, uint8_t * data, int dt_len);
void co_pdo_Send1(uint32_t n, uint8_t node, uint8_t * data, int dt_len);


#endif /* PDO_H_ */
