/*! \file co_main.h \brief Main CanOpen master state machine module. */
//*****************************************************************************
//
// File Name	: 'co_main.h'
// Title		: Main CanOpen state machine module.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
///	\ingroup CanOpen
/// \defgroup 
//
//*****************************************************************************
#ifndef CO_MAIN_H_
#define CO_MAIN_H_

#include "main.h"


#define		CO_NODEMASK 	0x0000007F
#define		CO_CODEMASK 	0x00000780

/** @name CANopen message ID definitions.
 *  RX and TX are referred to device node (server)
 */
///@{
#define NMT_FCODE		0x00	//!< Network management ID
#define SYNC_FCODE		0x080	//!< SYNC messages ID
#define TIME_FCODE		0x100	//!< Timestamp messages ID
#define EMCY_FCODE		0x080	//!< Emergency messages ID
#define PDO1TX_FCODE	0x180	//!< Transmit PDO1 messages ID
#define PDO1RX_FCODE	0x200	//!< Receive PDO1 messages ID
#define PDO2TX_FCODE	0x280	//!< Transmit PDO2 messages ID
#define PDO2RX_FCODE	0x300	//!< Receive PDO2 messages ID
#define PDO3TX_FCODE	0x380	//!< Transmit PDO3 messages ID
#define PDO3RX_FCODE	0x400	//!< Receive PDO3 messages ID
#define PDO4TX_FCODE	0x480	//!< Transmit PDO4 messages ID
#define PDO4RX_FCODE	0x500	//!< Receive PDO4 messages ID
#define SDOTX_FCODE		0x580	//!< Transmit SDO messages ID
#define SDORX_FCODE		0x600	//!< Receive SDO messages ID
#define NMTERR_FCODE	0x700	//!< Network management error and information messages ID

//#define PDO21TX_FCODE	0x
///@}

#define ID_MASK						0x780	//!< Mask of CANopen messages ID
#define NODEID_MASK					0x07F	//!< Mask of CANopen messages ID
#define ALL_ACCEPT_ID_MASK			0x7FF	//!< Mask of CANopen messages ID

/**
 * \struct co_msg_t
 * \brief Structure to hold CANopen message data and parameter
 *
 */
typedef struct str_co_msg_t {
	uint32_t tx_fid;		//!< COB-ID of the transmitted can message
	uint32_t rx_fid;		//!< COB-ID of the server answer (if any)
	uint8_t node;			//!< node id of the client
	uint32_t index;			//!< index of the server object dictionary to addressed
	uint8_t subindex;		//!< sub index of the server object dictionary
	char r_w;				//!< field indicating the action to be performed: 'r' read, 'w' write
	uint8_t * data_out;		//!< buffer for the data field of the CanOpen message
	uint8_t * data_in;		//!< buffer for the data transmitted by the server
	uint8_t datalen;		//!< can frame data length. Depends from the type of message.
}co_msg_t;

// call back array
#define CO_MAIN_CBACK_ARRAY_SIZE	16	//!< Max number of element in the callback array
//typedef void (*p_cback_t)(void);		//!< Typedef for the callback function pointers
enum{
	NOT_TYPED,
	DOSIMETER_TYPE,
	MOTOR_TYPE,
	COLLIMATOR_TYPE,
};
typedef struct{ 
	void (*p_cback)(void);		//!< Typedef for the callback function pointers
	int tipo;
}p_cback_t;

//!< Initialize the CanOpen management machine.
uint32_t co_main_Init(FDCAN_HandleTypeDef *p_can, TIM_HandleTypeDef *htim);
//!< Add callback function to the callback array.
int co_main_AddCback(p_cback_t pcb);
//!< Set the SYNC interval in ms
void co_main_StartSync(uint32_t ms);
void co_main_StopSync(void);
//!< Set the HEARTBEAT interval in ms
void co_main_SetHbeat(uint32_t ms);

/*
//!< Polls motors and collimator management functions.
void co_main_Process(void);
uint32_t co_main_GetHbeatInterval(void);
uint32_t co_main_Init_CAN2(void);
*/

void TC_Canopen_Handler(TIM_HandleTypeDef *htim);

#endif /* CO_MAIN_H_ */
