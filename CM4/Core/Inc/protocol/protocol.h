/*
 * Protocol.h
 *
 * Created: 27/02/2015 10:30:32
 *  Author: Alessandro
 */ 

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

typedef enum {
	COM_IPC = 0,
	COM_LAST  // per marcare il numero di canali usati
}eCom_t;


void ipc_protocol_Init(void);

#endif /* PROTOCOL_H_ */
