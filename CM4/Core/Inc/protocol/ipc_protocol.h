/*
 * ipc_protocol.h
 *
 *  Created on: 28 feb 2022
 *      Author: stefano
 *
 *      Header to process IPC command on CM4
 */

#ifndef INC_PROTOCOL_IPC_PROTOCOL_H_
#define INC_PROTOCOL_IPC_PROTOCOL_H_

#include "IPC_common.h"

int ipc_protocol_Process(cmd_msg_t *cmd);

#endif /* INC_PROTOCOL_IPC_PROTOCOL_H_ */
