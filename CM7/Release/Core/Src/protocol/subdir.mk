################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/protocol/binaryprotocol.c \
../Core/Src/protocol/dbgprotocol.c \
../Core/Src/protocol/protocol.c \
../Core/Src/protocol/stxetx.c 

OBJS += \
./Core/Src/protocol/binaryprotocol.o \
./Core/Src/protocol/dbgprotocol.o \
./Core/Src/protocol/protocol.o \
./Core/Src/protocol/stxetx.o 

C_DEPS += \
./Core/Src/protocol/binaryprotocol.d \
./Core/Src/protocol/dbgprotocol.d \
./Core/Src/protocol/protocol.d \
./Core/Src/protocol/stxetx.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/protocol/%.o Core/Src/protocol/%.su: ../Core/Src/protocol/%.c Core/Src/protocol/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -DCORE_CM7 -DUSE_HAL_DRIVER -DSTM32H757xx -c -I../Core/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc -I../../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../../Drivers/CMSIS/Include -I../../Middlewares/Third_Party/FreeRTOS/Source/include -I../../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../LWIP/App -I../LWIP/Target -I../../Middlewares/Third_Party/LwIP/src/include -I../../Middlewares/Third_Party/LwIP/system -I../../Drivers/BSP/Components/lan8742 -I../../Middlewares/Third_Party/LwIP/src/include/netif/ppp -I../../Middlewares/Third_Party/LwIP/src/include/lwip -I../../Middlewares/Third_Party/LwIP/src/include/lwip/apps -I../../Middlewares/Third_Party/LwIP/src/include/lwip/priv -I../../Middlewares/Third_Party/LwIP/src/include/lwip/prot -I../../Middlewares/Third_Party/LwIP/src/include/netif -I../../Middlewares/Third_Party/LwIP/src/include/compat/posix -I../../Middlewares/Third_Party/LwIP/src/include/compat/posix/arpa -I../../Middlewares/Third_Party/LwIP/src/include/compat/posix/net -I../../Middlewares/Third_Party/LwIP/src/include/compat/posix/sys -I../../Middlewares/Third_Party/LwIP/src/include/compat/stdc -I../../Middlewares/Third_Party/LwIP/system/arch -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/mdk-arm -I../../Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg -I../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include/metal/compiler/gcc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-protocol

clean-Core-2f-Src-2f-protocol:
	-$(RM) ./Core/Src/protocol/binaryprotocol.d ./Core/Src/protocol/binaryprotocol.o ./Core/Src/protocol/binaryprotocol.su ./Core/Src/protocol/dbgprotocol.d ./Core/Src/protocol/dbgprotocol.o ./Core/Src/protocol/dbgprotocol.su ./Core/Src/protocol/protocol.d ./Core/Src/protocol/protocol.o ./Core/Src/protocol/protocol.su ./Core/Src/protocol/stxetx.d ./Core/Src/protocol/stxetx.o ./Core/Src/protocol/stxetx.su

.PHONY: clean-Core-2f-Src-2f-protocol

