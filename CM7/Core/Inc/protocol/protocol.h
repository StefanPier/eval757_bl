/*
 * Protocol.h
 *
 * Created: 27/02/2015 10:30:32
 *  Author: Alessandro
 */ 

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

typedef enum {
	COM_SOCKET0 = 0,
	COM_SOCKET1,
	COM_SOCKET2,
	COM_SOCKET3,
	COM_SOCKET4,
	COM_BLUET,
	COM_DBG,
	COM_USART1,
	COM_485,
	COM_USB,
	
	COM_LAST  // per marcare il numero di canali usati
}eCom_t;


void protocol_Init(void);
//void protocol_Process(void);
void ProtocolTask(void const * arg);

#endif /* PROTOCOL_H_ */
