/*
 * dbgprotocol.h
 *
 * Created: 27/02/2015 14:58:08
 *  Author: Alessandro
 */ 


#ifndef DBGPROTOCOL_H_
#define DBGPROTOCOL_H_

#include "buffer.h"
#include "protocol/protocol.h"

void dbgprotocolInit ( cBuffer* buff);
void dbgprotocol_Debug(eCom_t eComm);


#endif /* DBGPROTOCOL_H_ */