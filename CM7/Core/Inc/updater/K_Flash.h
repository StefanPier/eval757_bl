/*
 * K_Flash.hpp
 *
 *  Created on: 24 ott 2019
 *      Author: user
 */

#ifndef K_FLASH_H_
#define K_FLASH_H_

//Flash function

#ifdef __cplusplus
 extern "C" {
#endif

/* Error code */
enum
{
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
};

/* protection type */
enum{
  FLASHIF_PROTECTION_NONE         = 0,
  FLASHIF_PROTECTION_PCROPENABLED = 0x1,
  FLASHIF_PROTECTION_WRPENABLED   = 0x2,
  FLASHIF_PROTECTION_RDPENABLED   = 0x4,
};

/* protection update */
enum {
	FLASHIF_WRP_ENABLE,
	FLASHIF_WRP_DISABLE
};

//!Info Structure to erase sector on flash
typedef struct __attribute__((__packed__)) {
	uint32_t bank;
	uint32_t sector;
	uint32_t nsector;
}erase_sector_t;


//Sector 128kb bank1
#define FLASH_SECTOR_NUMBER			1
#define INTERNAL_FLASH_PAGE_SIZE	32
#define INTERNAL_WORDS_IN_PAGE		(INTERNAL_FLASH_PAGE_SIZE/4)

uint32_t FLASH_WriteProtectionConfig(uint32_t protectionstate);
uint32_t FLASH_If_GetWriteProtectionStatus(void);
void FLASH_If_Init(void);
void Flash_If_CheckAndUpdateOptionBytes(void);
uint32_t FLASH_If_Write(uint32_t destination, uint32_t *p_source, uint32_t length);
uint32_t FLASH_If_Erase(uint32_t);
//uint32_t FLASH_If_EraseSector(uint32_t bank, uint32_t sector, uint32_t nsector);
uint32_t FLASH_If_EraseSector(erase_sector_t*);


#ifdef __cplusplus
}
#endif

#endif /* K_FLASH_H_ */
