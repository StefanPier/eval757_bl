/*
 * updater.h
 *
 *  Created on: 22 apr 2022
 *      Author: stefano
 */

#ifndef INC_UPDATER_UPDATER_H_
#define INC_UPDATER_UPDATER_H_

#include "main.h"
#include "updater_settings.h"

int erase_operation(update_erase_t*);
int erase_all_operation(update_erase_all_t*);
int write_operation(update_write_t*);
int read_operation(update_read_t*);
int check_operation(app_info_t*);
int checksha256(uint8_t *hash1,uint8_t *hash2);
int go_operation(uint32_t);
void DeInitAndReset(void);

#endif /* INC_UPDATER_UPDATER_H_ */
