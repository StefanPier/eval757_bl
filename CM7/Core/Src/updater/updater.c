/*
 * updater.c
 *
 *  Created on: 22 apr 2022
 *      Author: stefano
 */

#include "updater.h"
#include "settings.h"
#include "main.h"
#include "K_Flash.h"

extern CRC_HandleTypeDef hcrc;
extern HASH_HandleTypeDef hhash;
extern UART_HandleTypeDef huart1;
extern ETH_HandleTypeDef heth;
extern TIM_HandleTypeDef htim1;
extern internal_bl_cmd_t internal_bl_cmd;

/** @brief Erase sector command
 *
 *  @param	uint32_t* pRxData: pointer to sector and bank
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int erase_operation(update_erase_t* pRxData){
	uint32_t bank = pRxData->bank;
	//Check correct bank
	if( (bank != (pRxData->bank_c ^ 0xFFFFFFFFU) ) || ( bank!= FLASH_BANK_1 && bank!= FLASH_BANK_2) )
	{
		return RES_FAIL;
	}
	uint32_t sector = pRxData->sector;
	//Check correct sector
	if(sector != (pRxData->sector_c ^ 0xFFFFFFFFU) || sector>FLASH_SECTOR_7){
		return RES_FAIL;
	}

	uint32_t nsector = pRxData->nsector;
	//Check correct number of sectors to be erased
	if(nsector != (pRxData->nsector_c ^ 0xFFFFFFFFU) || nsector>FLASH_SECTOR_TOTAL){
		return RES_FAIL;
	}

	erase_sector_t SectorToErase;
	SectorToErase.bank = bank;
	SectorToErase.sector = sector;
	SectorToErase.nsector = nsector;
	uint32_t res_erase= FLASH_If_EraseSector(&SectorToErase);
	if ( res_erase!=0xFFFFFFFFU ){
		return RES_FAIL;
	}
	return RES_SUCCESS;
}

/** @brief Erase all sector command
 *
 *  @param	uint32_t* pRxData: pointer to sector and bank
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int erase_all_operation(update_erase_all_t* pRxData){
	uint32_t start = pRxData->start_address;
	uint32_t stop = pRxData->stop_address;
	//Check correct bank
	if( (start != (pRxData->start_address_c ^ 0xFFFFFFFFU) ) || (stop != (pRxData->stop_address_c ^ 0xFFFFFFFFU) )  )
	{
		return RES_FAIL;
	}
	//Check correct sector
	if(start >= stop || stop>FLASH_END || start< FLASH_BASE){
		return RES_FAIL;
	}

	//Calculate sectors to erase
	erase_sector_t SectorToErase_start;
	erase_sector_t SectorToErase_stop;
	//Calculate Sector To erase for start address
	if(start<FLASH_BANK2_BASE){
		SectorToErase_start.bank = FLASH_BANK_1;
		SectorToErase_start.sector = (FLASH_BANK1_BASE-start) / FLASH_SECTOR_SIZE;
	}else{
		SectorToErase_start.bank = FLASH_BANK_2;
		SectorToErase_start.sector = (FLASH_BANK2_BASE-start) / FLASH_SECTOR_SIZE;
	}
	if(SectorToErase_start.sector>FLASH_SECTOR_7)
		SectorToErase_start.sector=FLASH_SECTOR_7;
	/*
	SectorToErase_start.nsector = (SectorToErase_start.sector-FLASH_SECTOR_7)+1;
	*/
	//Calculate Sector To erase for stop address
	if(stop<FLASH_BANK2_BASE){
		SectorToErase_stop.bank = FLASH_BANK_1;
		SectorToErase_stop.sector = (FLASH_BANK1_BASE-stop) / FLASH_SECTOR_SIZE;
	}else{
		SectorToErase_stop.bank = FLASH_BANK_2;
		SectorToErase_stop.sector = (FLASH_BANK2_BASE-stop) / FLASH_SECTOR_SIZE;
	}
	if(SectorToErase_stop.sector>FLASH_SECTOR_7)
		SectorToErase_stop.sector=FLASH_SECTOR_7;

	/*
	SectorToErase_stop.nsector = (SectorToErase_stop.sector-FLASH_SECTOR_7)+1;
	*/


	uint32_t res_erase;
	//Erase bank2 if start address is below that threshold address
	if(SectorToErase_start.bank==SectorToErase_stop.bank){
		erase_sector_t SectorToErase_union;
		SectorToErase_union.bank = SectorToErase_start.bank;
		SectorToErase_union.sector = SectorToErase_start.sector;
		SectorToErase_union.nsector = SectorToErase_stop.sector-SectorToErase_start.sector+1;
		res_erase= FLASH_If_EraseSector(&SectorToErase_union);
		if ( res_erase!=0xFFFFFFFFU ){
			return RES_FAIL;
		}
	}else{
		//Erase bank2 if stop sector is the last of bank 2
		if(SectorToErase_stop.sector=FLASH_SECTOR_7){
			if(FLASH_If_Erase(FLASH_BANK_2)!=FLASHIF_OK){
				return RES_FAIL;
			}
		}else{
			//Partial Erase bank 2
			SectorToErase_stop.nsector = SectorToErase_stop.sector-FLASH_SECTOR_0 + 1;
			SectorToErase_stop.sector = FLASH_SECTOR_0;
			res_erase= FLASH_If_EraseSector(&SectorToErase_stop);
			if ( res_erase!=0xFFFFFFFFU ){
				return RES_FAIL;
			}

		}
		//Partial erase bank1
		SectorToErase_start.nsector = FLASH_SECTOR_7 - SectorToErase_start.sector + 1;
		res_erase= FLASH_If_EraseSector(&SectorToErase_start);
		if ( res_erase!=0xFFFFFFFFU ){
			return RES_FAIL;
		}
	}

	return RES_SUCCESS;
}

/** @brief Write byte command on Flash
 *
 *  @param	update_write_t* pRxData: pointer to data to write on Flash
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int write_operation(update_write_t* pRxData){
	if(pRxData->wsize % INTERNAL_FLASH_PAGE_SIZE != 0){
		return RES_FAIL;
	}
	if( pRxData->waddress < FLASH_BASE ||  pRxData->waddress > FLASH_END )
	{
		return RES_FAIL;
	}
	if(pRxData->wsize > sizeof(pRxData->wdata)){
		return RES_FAIL;
	}
	uint32_t status=FLASH_If_Write(pRxData->waddress,(uint32_t *)pRxData->wdata,pRxData->wsize/sizeof(uint32_t));
	if( status!=FLASHIF_OK)
		return RES_FAIL;
	else
		return RES_SUCCESS;
}

/** @brief Read byte command from Flash
 *
 *  @param	update_read_t* pRxData: pointer to data to read from Flash
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int read_operation(update_read_t* pRxData){
	if( pRxData->raddress < FLASH_BASE ||  pRxData->raddress > FLASH_END )
	{
		return RES_FAIL;
	}
	if(pRxData->rsize > sizeof(pRxData->rdata)){
		return RES_FAIL;
	}
	uint8_t * raddr = (uint8_t *)pRxData->raddress;
	for(int i=0;i<pRxData->rsize;i++){
		pRxData->rdata[i]=*(raddr+i);
	}
	return RES_SUCCESS;
}

/**
 *
 * @param hash1
 * @param hash2
 * @return 1 se uguali, 0 se diversi
 */
//__attribute__ ((__section__(".ramfunc")))
int checksha256(uint8_t *hash1,uint8_t *hash2){
	for(uint8_t i=0; i<8; i++){
		if(hash1[i]!=hash2[i]){
			return RES_FAIL;
		}
	}
	return RES_SUCCESS;
}

/** @brief Check FW command on Flash
 *
 *  @param	app_info_t* pInfoData: pointer to info structur to check FW
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int check_operation(app_info_t* pInfoApp){
	if( pInfoApp->CodeAddressCM4 < FLASH_BASE ||  pInfoApp->CodeAddressCM4 > FLASH_END )
	{
		return RES_FAIL;
	}
	if( pInfoApp->CodeAddressCM7 < FLASH_BASE ||  pInfoApp->CodeAddressCM7 > FLASH_END )
	{
		return RES_FAIL;
	}
	if( pInfoApp->CodeAddressCM7 + pInfoApp->lengthCM7 > pInfoApp->CodeAddressCM4 || pInfoApp->CodeAddressCM4 + pInfoApp->lengthCM4 > FLASH_END )
		return RES_FAIL;
	uint32_t addr;
	uint32_t len;
	uint8_t hash[32];
	if(pInfoApp->id_core == CM7_ID){
		addr = pInfoApp->CodeAddressCM7;
		len = pInfoApp->lengthCM7;
	}else if(pInfoApp->id_core == CM4_ID){
		addr = pInfoApp->CodeAddressCM4;
		len = pInfoApp->lengthCM4;
	}else
		return RES_FAIL;

	//Hash of the FW to check
	if(HAL_HASHEx_SHA256_Start(&hhash,((uint8_t*)addr)+sizeof(app_info_t),len,hash,10000)!=HAL_OK)
		return RES_FAIL;

	int res = checksha256(hash,(uint8_t*)(pInfoApp->app_sha256));

	return res;
}

/** @brief Global deinitialization and reboot,
 *
 *  @param	uint32_t goaddr: address of function to jump
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
void DeInitAndReset(void)
{
	__disable_irq();
	HAL_ETH_DeInit(&heth);
	HAL_UART_DeInit(&huart1);
	HAL_HASH_DeInit(&hhash);
	HAL_CRC_DeInit(&hcrc);
	HAL_TIM_Base_DeInit(&htim1);
	HAL_SuspendTick();
	HAL_RCC_DeInit();
	K_WD_ReloadTimer();
	HAL_NVIC_SystemReset();
}

/** @brief Go command,
 *
 *  @param	uint32_t goaddr: address of function to jump
 *  @return RES_FAIL: if KO
 *  		RES_SUCCESS: if OK
 */
int go_operation(uint32_t goaddr){
	if( goaddr < FLASH_BASE ||  goaddr > FLASH_END )
	{
		return RES_FAIL;
	}
	internal_bl_cmd.cmd = Force_Start_App;
	internal_bl_cmd.not_cmd = ~Force_Start_App;
	internal_bl_cmd.equal_cmd = Force_Start_App;
	internal_bl_cmd.crc = HAL_CRC_Calculate(&hcrc,(uint32_t *)&internal_bl_cmd,sizeof(internal_bl_cmd_t)-4);
	return RES_SUCCESS;
	/*
	//Resetting
	//Comando il lancio dell'applicativo al prox riavvio:
	__disable_irq();
	DeInitAndReset();
	*/
}

