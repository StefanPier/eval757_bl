/*
 * K_Flash.cpp
 *
 *  Created on: 24 ott 2019
 *      Author: Stefano Pieri
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <updater/K_Flash.h>
#include "main.h"
/*
#include "stm32h7xx_hal.h"
#include "usart.h"
*/
/**
  * @brief  This function does an erase of all user flash area
  * @param  start: start of user flash area
  * @retval FLASHIF_OK : user flash area successfully erased
  *         FLASHIF_ERASEKO : error occurred
  */
uint32_t FLASH_If_Erase(uint32_t bank)
{
  FLASH_EraseInitTypeDef desc;
  uint32_t result = FLASHIF_OK;

  HAL_FLASH_Unlock();

  //Mass Erase
  desc.TypeErase = FLASH_TYPEERASE_MASSERASE;
  desc.Banks=bank;
  desc.VoltageRange=FLASH_VOLTAGE_RANGE_4;

  if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK)
  {
	  result = FLASHIF_ERASEKO;
  }

  HAL_FLASH_Lock();

  return result;
}


/**
  * @brief  This function does an erase of a sector of user flash area
  * @param  erase_sector_t* pSectorToErase: Structure to Erase Sector (bank, sector, nsector)
  * @param  uint32_t sector: sector to be erased
  * @retval FLASHIF_OK : user flash area successfully erased
  *         FLASHIF_ERASEKO : error occurred
  */
uint32_t FLASH_If_EraseSector(erase_sector_t* pSectorToErase)
{
  FLASH_EraseInitTypeDef desc;
  uint32_t result = FLASHIF_OK;

  HAL_FLASH_Unlock();

  //Sector Erase
  desc.TypeErase = FLASH_TYPEERASE_SECTORS;
  desc.Banks = pSectorToErase->bank;
  desc.Sector = pSectorToErase->sector;
  desc.NbSectors = pSectorToErase->nsector;
  desc.VoltageRange=FLASH_VOLTAGE_RANGE_4;

  if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK)
  {
	  result = FLASHIF_ERASEKO;
  }

  HAL_FLASH_Lock();

  return result;
}

/**
  * @brief  Configure the write protection status of user flash area.
  * @param  protectionstate : FLASHIF_WRP_DISABLE or FLASHIF_WRP_ENABLE the protection
  * @retval uint32_t FLASHIF_OK if change is applied.
  */
uint32_t FLASH_WriteProtectionConfig(uint32_t protectionstate)
{
  FLASH_OBProgramInitTypeDef config_new, config_old;
  HAL_StatusTypeDef result;

  /* Get the current configuration */
  config_old.Banks = FLASH_BANK_1;
  HAL_FLASHEx_OBGetConfig( &config_old );

  /* The parameter says whether we turn the protection on or off */
  config_new.WRPState = (protectionstate == FLASHIF_WRP_ENABLE ? OB_WRPSTATE_ENABLE : OB_WRPSTATE_DISABLE);

  /* We want to modify only the Write protection */
  config_new.OptionType = OPTIONBYTE_WRP;

  /* No read protection, keep BOR and reset settings */
  config_new.RDPLevel = OB_RDP_LEVEL0;
  config_new.BORLevel = config_old.BORLevel;
  config_new.USERConfig = config_old.USERConfig;

  config_new.WRPSector=OB_WRP_SECTOR_0;

  /* Initiating the modifications */
  result = HAL_FLASH_OB_Unlock();

  /* program if unlock is successful */
  if (result == HAL_OK)
  {
    result = HAL_FLASHEx_OBProgram(&config_new);
  }

  return (result == HAL_OK ? FLASHIF_OK: FLASHIF_PROTECTION_ERRROR);
}




/**
  * @brief  Returns the write protection status of application flash area.
  * @param  None
  * @retval If a sector in application area is write-protected returned value is a combinaison
            of the possible values : FLASHIF_PROTECTION_WRPENABLED, FLASHIF_PROTECTION_PCROPENABLED, ...
  *         If no sector is write-protected FLASHIF_PROTECTION_NONE is returned.
  */
uint32_t FLASH_If_GetWriteProtectionStatus(void)
{
  FLASH_OBProgramInitTypeDef config;
  uint32_t wrp1_status = 0;
  uint32_t protection = FLASHIF_PROTECTION_NONE;

  config.Banks=FLASH_BANK_1;
  /* Get the current configuration */
  HAL_FLASHEx_OBGetConfig( &config );

  wrp1_status = config.WRPSector;

  /* Final evaluation of status */
  if (wrp1_status != 0)
  {
	  protection = FLASHIF_PROTECTION_WRPENABLED;
  }

  return protection;
}



/**
  * @brief  Unlocks Flash for write access
  * @param  None
  * @retval None
  */
void FLASH_If_Init(void)
{
  /* Unlock the Program memory */
  HAL_FLASH_Unlock();

  /* Clear all FLASH flags */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_BANK1);
  /* Unlock the Program memory */
  HAL_FLASH_Lock();
}

/**
 * Check current option bytes configuration and change it
 * in case of disalignment
 */
void Flash_If_CheckAndUpdateOptionBytes(void){
	FLASH_OBProgramInitTypeDef obInit;
	HAL_FLASHEx_OBGetConfig(&obInit);
	if(obInit.BORLevel!=OB_BOR_LEVEL3){//Se non ho la soglia di brown out corretta la sistemo (desiderata 2.3V)
		obInit.OptionType=OPTIONBYTE_BOR;
		obInit.BORLevel=OB_BOR_LEVEL3;//Lo imposto a 2
	    if(HAL_FLASH_Unlock()==HAL_OK){
	    	if(HAL_FLASH_OB_Unlock()==HAL_OK){
	    		if(HAL_FLASHEx_OBProgram(&obInit)==HAL_OK){
	    			HAL_FLASH_OB_Launch();
	    		}
	    		if(HAL_FLASH_OB_Lock()!=HAL_OK){
	    			Error_Handler();
	    		}
	    	}
    		if(HAL_FLASH_Lock()!=HAL_OK){
    			Error_Handler();
    		}
	    }
	}
}

/* Public functions ---------------------------------------------------------*/
/**
  * @brief  This function writes a data buffer in flash (data are 32-bit aligned).
  * @note   After writing data buffer, the flash content is checked.
  * @param  destination: start address for target location
  * @param  p_source: pointer on buffer with data to write
  * @param  length: length of data buffer (unit is 32-bit word)
  * @retval uint32_t 0: Data successfully written to Flash memory
  *         1: Error occurred while writing data in Flash memory
  *         2: Written Data in flash memory is different from expected one
  */
uint32_t FLASH_If_Write(uint32_t destination, uint32_t *p_source, uint32_t length)
{
  uint32_t status = FLASHIF_OK;
  uint32_t *p_actual = p_source; /* Temporary pointer to data that will be written in a half-page space */
  uint32_t i = 0;

  HAL_FLASH_Unlock();

  HAL_StatusTypeDef res_prog=HAL_OK;
  while (p_actual < (uint32_t*)(p_source + length))
  {
    /* Write the buffer to the memory */
	res_prog=HAL_FLASH_Program(FLASH_TYPEPROGRAM_FLASHWORD, destination, (uint32_t)p_actual);
    if (res_prog == HAL_OK) /* No error occurred while writing data in Flash memory */
    {
      /* Check if flash content matches memBuffer */
      for (i = 0; i < INTERNAL_WORDS_IN_PAGE; i++)
      {
        if ((*(uint32_t*)(destination + 4 * i)) != p_actual[i])
        {
          /* flash content doesn't match memBuffer */
          status = FLASHIF_WRITINGCTRL_ERROR;
          break;
        }
      }

      /* Increment the memory pointers */
      destination += INTERNAL_FLASH_PAGE_SIZE;
      p_actual += INTERNAL_WORDS_IN_PAGE;
    }
	else
	{
		status = FLASHIF_WRITING_ERROR;
	}

    if ( status != FLASHIF_OK )
    {
      break;
    }
  }

  HAL_FLASH_Lock();

  return status;
}

