/*
 * binaryprotocol.c
 *
 * Created: 27/02/2015 11:01:14
 *  Author: Alessandro
 */ 

#include <manage_debug.h>
#include <string.h>
#include "binaryprotocol.h"
#include "main.h"
#include "updater_settings.h"


static char tmpstr[N_MAX_DATA_LENGHT];

/**
  * @brief 	Retrieve separated parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of separated params
  * @param	char separator: char separator between parameters
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return >=1 = success, 0 = fail
  */
static int list_GetNextParam(unsigned char * src, char separator, char * param)
{
	char ctmp;
	uint32_t i = 0;
	int len_src=strlen((const char *)src);
	
	while(i < len_src)
	{
		ctmp = src[i];
		if((ctmp == separator)||(ctmp == 0))
		break;
		param[i] = ctmp;
		i++;
	}
	
	param[i] = 0;
	strcpy((char *)src, (char *)(src+i+1)); // cut out the extracted param
	return i;
}

/**
  * @brief 	Retrieve comma separated parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of comma separated params
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return >=1 = success, 0 = fail
  */
static int list_command_GetNextParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, ',' ,param);
}

/**
  * @brief 	Retrieve parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of point separated ip params (es. 169.254.1.77)
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return ( >=1 = success, 0 = fail)
  */
static int list_GetNextIpParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, '.' ,param);
}

/**
  * @brief 	Command to communicate FW version
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Unused Parameter
  * @return None
  */
extern bootloader_ver_t reset_bootloader_info_coded;

static void verfwcmd (unsigned char trunk, unsigned char* dataptrvoid)
{
	char tmp[20];
	sprintf(tmpstr,"%d.%d%d", reset_bootloader_info_coded.Ver1, reset_bootloader_info_coded.Ver2,reset_bootloader_info_coded.Ver3);
	//	strcpy(tmpstr, "1.55");
	
	debug_printf(DEBUG_BYNARY_PROTOCOL,"CMD FW version: %s\r\n",tmpstr);
	command_send(trunk, 1, FWVERSION_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}


/**
  * @brief 	Command to erase sector on flash
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Bank, sector, number of sectors
  * @return None
  */
static void erasefwcmd (unsigned char trunk, unsigned char* dataptrvoid)
{
	if(erase_operation((update_erase_t*)dataptrvoid) == RES_SUCCESS)
		strcpy(tmpstr, "ACK");
	else
		strcpy(tmpstr, "NAK");

	debug_printf(DEBUG_BYNARY_PROTOCOL,"ERASE_CMD: %s\r\n",tmpstr);
	command_send(trunk, 1, ERASE_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

//Send Start programming address, Stop Programming Address
/**
  * @brief 	Command to erase all sector on flash from start address to stop address
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Start and stop addresses
  * @return None
  */
static void eraseallfwcmd (unsigned char trunk, unsigned char* dataptrvoid)
{
	if(erase_all_operation((update_erase_all_t*)dataptrvoid) == RES_SUCCESS)
		strcpy(tmpstr, "ACK");
	else
		strcpy(tmpstr, "NAK");

	debug_printf(DEBUG_BYNARY_PROTOCOL,"ERASE_CMD: %s\r\n",tmpstr);
	command_send(trunk, 1, ERASE_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
  * @brief 	Command to write a data array on flash
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Address, size and data to write
  * @return None
  */
static void writefwcmd (unsigned char trunk, unsigned char* dataptrvoid){
	if(write_operation((update_write_t*)dataptrvoid) == RES_SUCCESS)
		strcpy(tmpstr, "ACK");
	else
		strcpy(tmpstr, "NAK");

	debug_printf(DEBUG_BYNARY_PROTOCOL,"WRITE_CMD: %s\r\n",tmpstr);
	command_send(trunk, 1, WRITE_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
  * @brief 	Command to read a data array from flash
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Address and size to read
  * @return None
  */
static void readfwcmd (unsigned char trunk, unsigned char* dataptrvoid){
	int countbyte;
	update_read_t read;
	update_read_t *pread = (update_read_t *)dataptrvoid;
	read.raddress = pread->raddress;
	read.rsize = pread->rsize;
	if(read_operation((update_read_t*)&read) != RES_SUCCESS){
		strcpy(tmpstr, "NAK");
		countbyte=strlen(tmpstr)+1;
		debug_printf(DEBUG_BYNARY_PROTOCOL,"READ_CMD: %s\r\n",tmpstr);
	}else{
		memcpy(tmpstr, (void*) &read, sizeof(update_read_t));
		countbyte = sizeof(update_read_t);
	}

	command_send(trunk, 1, READ_CMD, countbyte, (unsigned char *)tmpstr);
}

/**
  * @brief 	Command to check FW coherence
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	structure of FW to check
  * @return None
  */
static void checkfwcmd (unsigned char trunk, unsigned char* dataptrvoid){
	int countbyte;
	app_info_t *pcheck = (app_info_t *)dataptrvoid;
	if(check_operation(pcheck) != RES_SUCCESS){
		strcpy(tmpstr, "NAK");
		countbyte=strlen(tmpstr)+1;
	}else{
		strcpy(tmpstr, "ACK");
		countbyte=strlen(tmpstr)+1;
	}
	debug_printf(DEBUG_BYNARY_PROTOCOL,"CHECK_CMD: %s\r\n",tmpstr);

	command_send(trunk, 1, CHECK_CMD, countbyte, (unsigned char *)tmpstr);
}

/**
  * @brief 	Command to jump on FW address
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	address to jump
  * @return None
  */
static void gofwcmd (unsigned char trunk, unsigned char* dataptrvoid){
	int countbyte;
	uint32_t goaddr = *(uint32_t *)dataptrvoid;
	if(go_operation(goaddr) != RES_SUCCESS){
		strcpy(tmpstr, "NAK");
		countbyte=strlen(tmpstr)+1;
	}else{
		strcpy(tmpstr, "ACK");
		countbyte=strlen(tmpstr)+1;
	}
	debug_printf(DEBUG_BYNARY_PROTOCOL,"CHECK_CMD: %s\r\n",tmpstr);

	command_send(trunk, 1, CHECK_CMD, countbyte, (unsigned char *)tmpstr);
}

//******************************************************************//
//					END	 SECTION									//
//******************************************************************//



/**
 * \brief			Binary function initialization
 * 
 * \param			void
 * 
 * \return void
 */
void binaryproto_function_init (void)
{
	command_reset();
	command_init (FWVERSION_CMD, verfwcmd);
	command_init (ERASE_CMD, erasefwcmd);
	command_init (ERASEALL_CMD, eraseallfwcmd);
	command_init (WRITE_CMD, writefwcmd);
	command_init (READ_CMD, readfwcmd);
	command_init (CHECK_CMD, checkfwcmd);
	command_init (GO_CMD, gofwcmd);
	/*
	WRITE_PROTECTION,
	READ_UNPROTECTION,
	READ_PROTECTION,
	READ_UNPROTECTION,
	*/
}
