#include <manage_debug.h>
#include "main.h"
#include "cmsis_os.h"
#include "lwip.h"
#include "NetTask.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "updater_settings.h"
#include "updater.h"

extern sys_thread_t defaultTaskHandle;

static void close_tcp_netconn(struct netconn *nc);
static void netCallback(struct netconn *conn, enum netconn_evt evt, uint16_t length);
static void nonBlockingTCP(void *pvParameters);
static void set_tcp_server_netconn(struct netconn **nc, uint16_t port, netconn_callback callback);

//Queue Message from socket task
state_socket_pool_t state_socket_pool;

sys_thread_t ReceiveTaskHandle=NULL;

//Maximun event on tcp
#define EVENTS_QUEUE_SIZE 10
//TCP evnt struct
typedef struct {
    struct netconn *nc ;
    int type ;
} netconn_events;
//Storage of a queue
typedef struct{
	uint8_t QueueBuffer[ EVENTS_QUEUE_SIZE * sizeof( netconn_events ) ];
	StaticQueue_t QueueControlBlock;
}event_static_buffer_t;
/*
osMessageQueueAttr_t xQueueAttr_events;
osMessageQueueId_t xQueue_events;
*/
QueueHandle_t xQueue_events;
event_static_buffer_t NetTxQueueBufferPool_events;

/** @brief Return a circular buffer to use in ProtocolTask
 *
 *  @param	arg: socket index (equal to the Protocol trunk)
 *  @return Pointer to a circular uffer to decode protocol
 */
static cBuffer* getbyte_onsocket(void * arg){
	int trunk_idx = (int)arg;
	if(trunk_idx<0 || trunk_idx>=MAX_SOCKET_CONNECTION)
		Error_Handler();
	/*
	QueueHandle_t* NetRxQueueHandle_Pool = (QueueHandle_t*)&state_socket_pool.NetRxQueueHandle_Pool[trunk_idx];
	int socket_index;
	for (socket_index=0;socket_index<MAX_SOCKET_CONNECTION;socket_index++){
		if(&(state_socket_pool.NetRxQueueHandle_Pool[socket_index])==NetRxQueueHandle_Pool)
			break;
	}
	if(socket_index>=MAX_SOCKET_CONNECTION){
		Error_Handler();
		return NULL;
	}else
		return &state_socket_pool.protocol_socket_Rxbuffer[socket_index].circRxBuffer;
	*/
	return &state_socket_pool.protocol_socket_Rxbuffer[trunk_idx].circRxBuffer;
}

/** @brief Function to write a data on a protocol trunk
 *
 *  @param	arg: socket index (equal to the Protocol trunk)
 *  @return none
 */
static void write_byte_onsocket (unsigned char dato, void * arg)
{
	int trunk_idx = (int)arg;
	if(trunk_idx<0 || trunk_idx>=MAX_SOCKET_CONNECTION)
		Error_Handler();
#ifdef PROTOCOL_TX_ON_PIPE
	QueueHandle_t NetTxQueueHandle_Pool = (QueueHandle_t)state_socket_pool.NetTxQueueHandle_Pool[trunk_idx];
	/*
	int socket_index;
	for (socket_index=0;socket_index<MAX_SOCKET_CONNECTION;socket_index++){
		if(&(state_socket_pool.NetTxQueueHandle_Pool[socket_index])==NetTxQueueHandle_Pool)
			break;
	}
	if(socket_index>=MAX_SOCKET_CONNECTION){
		Error_Handler();
	}else{
	*/
	//Using Tx queue and TX task
	if(state_socket_pool.socket_state[trunk_idx].occuped==1){
		BaseType_t reply = xQueueSend(NetTxQueueHandle_Pool, &(dato), 0);
		if(reply!= pdTRUE){
			Error_Handler();
		}
	}
#else
		//Sending directly over IP
		if(state_socket_pool.socket_state[trunk_idx].occuped==1){
			err_t tx_err = netconn_write(state_socket_pool.socket_state[trunk_idx].associated_socket.conn,&dato,1,NETCONN_COPY);
			if(tx_err!=ERR_OK)
				Error_Handler();
		}
#endif
#ifdef PROTOCOL_TX_ON_PIPE
//	}
#endif
}

#ifdef PROTOCOL_TX_ON_PIPE
void write_onsocket (unsigned char dato, int trunk_idx){
	if(trunk_idx<0 || trunk_idx>=MAX_SOCKET_CONNECTION)
		Error_Handler();

	//Sending directly over IP
	if(state_socket_pool.socket_state[trunk_idx].occuped==1){
		err_t tx_err = netconn_write(state_socket_pool.socket_state[trunk_idx].associated_socket.conn,&dato,1,NETCONN_COPY);
		if(tx_err!=ERR_OK)
			Error_Handler();
	}
}
#endif

/** @brief Dummy function used on protocol task before tx data on trunk
 *
 *  @param	none
 *  @return none
 */
static void dummy_tx_enable_onsocket (int trunk_idx)
{
	if(trunk_idx>=0 && trunk_idx<MAX_SOCKET_CONNECTION){
		BaseType_t reply = xSemaphoreTake(state_socket_pool.NetTxQueueSem[trunk_idx], 10);
		if(reply!= pdTRUE){
			Error_Handler();
		}
	}
}

/** @brief Dummy function used on protocol task befor read protocol trunk
 *
 *  @param	none
 *  @return none
 */
static void dummy_rx_enable_onsocket (int trunk_idx)
{
	if(trunk_idx>=0 && trunk_idx<MAX_SOCKET_CONNECTION){
		if(uxSemaphoreGetCount(state_socket_pool.NetTxQueueSem[trunk_idx])==0){
			BaseType_t reply = xSemaphoreGive(state_socket_pool.NetTxQueueSem[trunk_idx]);
			/*
			if(reply!= pdTRUE){
				Error_Handler();
			}
			*/
		}
	}
}

/** @brief Delay function used on at the and of each process_packet
 *
 *  @param	none
 *  @return none
 */
static void delay_onsocket (void)
{

}

/*
 * Function to create queue of message in OS and buffer protocol and protocol channel initialization
 * For each possible communication socket (MAX_SOCKET_CONNECTION)
 */
/** @brief 	Function to create queue of message in OS and buffer protocol and protocol channel initialization
 *			For each possible communication socket (MAX_SOCKET_CONNECTION)
 *  @param	none
 *  @return none
 */
void MakeSocketQueues(){
	//General Section initialization
	state_socket_pool.available_socket=MAX_SOCKET_CONNECTION;
	//state_socket_pool.available_socket=1;
	for (int i=0;i<state_socket_pool.available_socket;i++){
		int num_char;
#ifdef PROTOCOL_TX_ON_PIPE
		//Tx Section Initialization:
		/* Create a queue capable of containing MAX_BYTE_FOR_SOCKET uint8_t values. */
		state_socket_pool.NetTxQueueHandle_Pool[i] = xQueueCreateStatic( MAX_BYTE_FOR_SOCKET,
														sizeof(uint8_t),
														state_socket_pool.NetTxQueueBuffer_Pool[i].QueueBuffer,
														&state_socket_pool.NetTxQueueBuffer_Pool[i].QueueControlBlock );
		if(state_socket_pool.NetTxQueueHandle_Pool[i]==NULL){
			Error_Handler();
		}
//		state_socket_pool.NetTxQueueSem[i]=xSemaphoreCreateBinary();
		state_socket_pool.NetTxQueueSem[i]=xSemaphoreCreateMutex();
		if(state_socket_pool.NetTxQueueSem[i]==NULL){
			Error_Handler();
		}
//		xSemaphoreGive(state_socket_pool.NetTxQueueSem[i]);
#endif
		//Rx Section Initialization:
	    /* Create a queue capable of containing MAX_BYTE_FOR_SOCKET uint8_t values. */
		state_socket_pool.NetRxQueueHandle_Pool[i] = xQueueCreateStatic( MAX_BYTE_FOR_SOCKET,
														sizeof(uint8_t),
														state_socket_pool.NetRxQueueBuffer_Pool[i].QueueBuffer,
														&state_socket_pool.NetRxQueueBuffer_Pool[i].QueueControlBlock );
		if(state_socket_pool.NetRxQueueHandle_Pool[i]==NULL){
			Error_Handler();
		}
		//Init RX Buffer for redundant protocol usage, and Protocol structure
		bufferInit( &state_socket_pool.protocol_socket_Rxbuffer[i].circRxBuffer, state_socket_pool.protocol_socket_Rxbuffer[i].dataRx, sizeof(state_socket_pool.protocol_socket_Rxbuffer[i].dataRx) );
		//Initializzation of the socket communication channel
		command_system_init(i, write_byte_onsocket, getbyte_onsocket, dummy_tx_enable_onsocket, dummy_rx_enable_onsocket, delay_onsocket, (void *)i);
		//Socket State initialization
		state_socket_pool.socket_state[i].occuped=0;
		state_socket_pool.socket_state[i].associated_socket.idx=-1;
		state_socket_pool.socket_state[i].associated_socket.conn=NULL;
	}
}

/** @brief Return an available socket channel index
 *
 *  @param	None
 *  @return Available socket index, -1 if not present.
 */
int getAvailableSocket(void){
	for (int i=0;i<MAX_SOCKET_CONNECTION;i++){
		if(state_socket_pool.socket_state[i].occuped==0)
			return i;
	}
	return -1;
}

/** @brief Return the socket channel index associated to a socket handle
 *
 *  @param	Item:	socket handle
 *  @return Socket index associated the socket handle
 *  		-1 if not present any socket index associated to the requested handle
 */
/*
int getAvailableSocket_fromNetconn(struct netconn * item){
	for (int i=0;i<MAX_SOCKET_CONNECTION;i++){
		if(state_socket_pool.socket_state[i].occuped==1)
			if(state_socket_pool.socket_state[i].associated_socket.conn==item)
				return i;
	}
	return -1;
}
*/
/** @brief Return the socket channel index associated to a netconn handle
 *
 *  @param	struct netconn * conn: handle of the connection to search the index of
 *  @return Socket index associated the netconn handle
 *  		-1 if not present any socket index associated to the requested handle
 */
int getSocketPoolIdx(struct netconn * conn){
	for (int i=0;i<MAX_SOCKET_CONNECTION;i++){
		if(state_socket_pool.socket_state[i].occuped==1){
			if(state_socket_pool.socket_state[i].associated_socket.conn==conn)
				return state_socket_pool.socket_state[i].associated_socket.idx;
		}
	}
	return -1;
}

/** @brief Set on the internal state socket pool the corrisponding new socket handle
 *
 *  @param	Item:	socket state to add (handle, available index)
 *  @return true if the socket is released correctly
 */
bool setAvailableSocket(accepted_socket_t item){
	int index = item.idx;
	if(index>=0 && index<MAX_SOCKET_CONNECTION){
		if(state_socket_pool.socket_state[index].occuped!=0){
			return false;
		}
		state_socket_pool.socket_state[index].occuped=1;
		state_socket_pool.socket_state[index].associated_socket=item;
		state_socket_pool.available_socket--;
	    char buf[50];
	    snprintf(buf, sizeof(buf), "Aggiunto socket %d, disponibili %d\r\n",
	    		index,state_socket_pool.available_socket);
	    debug_printf(DEBUG_NET,buf);
		if(state_socket_pool.available_socket<0)
			state_socket_pool.available_socket=0;
		return true;
	}else{
		return false;
	}
}

/** @brief Release queue OS associated with socket connection
 *
 *  @param	idx_pool: socket index to release
 *  @return none
 */
void flushSocketPool(int idx_pool){
	if(idx_pool<0)
		Error_Handler();
	xQueueReset(state_socket_pool.NetRxQueueHandle_Pool[idx_pool]);
	xQueueReset(state_socket_pool.NetTxQueueHandle_Pool[idx_pool]);
	state_socket_pool.socket_state[idx_pool].occuped=0;
	state_socket_pool.available_socket++;
    char buf[50];
    UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    snprintf(buf, sizeof(buf), "Liberato socket %d, disponibili %d\r\n",
    		idx_pool,state_socket_pool.available_socket);
    debug_printf(DEBUG_NET,buf);
	if(state_socket_pool.available_socket>MAX_SOCKET_CONNECTION)
		state_socket_pool.available_socket=MAX_SOCKET_CONNECTION;
}

/**
  * @brief  Initialize the HTTP server (start its thread)
  * 		Create EVENTS_QUEUE_SIZE Event Queues to manage
  * @param  none
  * @retval None
  */
void ManageNetTask(void const *argument)
{
	UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );

	/* init code for LWIP */
	MX_LWIP_Init();

	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );

	//Create a queue to store events on netconns

	/* Create a queue capable of containing 10 uint64_t values. */
	xQueue_events = xQueueCreateStatic( EVENTS_QUEUE_SIZE,
										sizeof(netconn_events),
										NetTxQueueBufferPool_events.QueueBuffer,
										&NetTxQueueBufferPool_events.QueueControlBlock);

	if(xQueue_events==NULL){
		Error_Handler();
	}
	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
	//Create task to manage connection event
	sys_thread_t res = sys_thread_new("lwiptest_noblock", nonBlockingTCP, NULL, 512 * 4, osPriorityAboveNormal);

	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
	///////////////////////////////////////////////////////////////////////////
/*
	  update_erase_t pRxData;
	  pRxData.bank = FLASH_BANK_1;
	  pRxData.bank_c = pRxData.bank ^ 0xFFFFFFFF;
	  pRxData.sector = FLASH_SECTOR_3;
	  pRxData.sector_c = pRxData.sector ^ 0xFFFFFFFF;
	  pRxData.nsector = 1;
	  pRxData.nsector_c = pRxData.nsector ^ 0xFFFFFFFF;
	  if(erase_operation(&pRxData) != RES_SUCCESS)
		  Error_Handler();
	/////////////////////////////////////////////////////////////////
	  extern HASH_HandleTypeDef hhash;
	  update_write_t pTxData;
	  update_write_t appTxData;
	  app_info_t *pInfoApp = (app_info_t *)appTxData.wdata;
	    //Simulate App in CM7
	  	memset((void *)pInfoApp, 0, sizeof(app_info_t));
		pInfoApp->id_core = CM7_ID;
		pInfoApp->CodeAddressCM7=FLASH_SECTOR_3*FLASH_SECTOR_SIZE + FLASH_BASE;
		pInfoApp->CodeAddressCM4=FLASH_SECTOR_3*FLASH_SECTOR_SIZE + 64*1024 +FLASH_BASE;
		pInfoApp->lengthCM7=128;
		  pTxData.waddress = pInfoApp->CodeAddressCM7 + sizeof(app_info_t);
		  pTxData.wsize = pInfoApp->lengthCM7;
	  for (int i=0;i<sizeof(pTxData.wdata);i++)
		  pTxData.wdata[i]=(uint8_t)rand();
	  if(HAL_HASHEx_SHA256_Start(&hhash,pTxData.wdata,pTxData.wsize,pInfoApp->app_sha256,10000)!=HAL_OK)
		  Error_Handler();
	  //Write app header
		appTxData.waddress = pInfoApp->CodeAddressCM7;
		appTxData.wsize = sizeof(app_info_t);
	  if(write_operation(&appTxData) != RES_SUCCESS)
		  Error_Handler();
	  //Write app data
	  if(write_operation(&pTxData) != RES_SUCCESS)
		  Error_Handler();

	  uint8_t *p_flash=(uint8_t *)(pTxData.waddress);
	  for(int i=0; i<pInfoApp->lengthCM7; i++){
		  if(p_flash[i] != pTxData.wdata[i] )
			  Error_Handler();
	  }

	  if(check_operation(pInfoApp)!=RES_SUCCESS)
		  Error_Handler();

	/////////////////////////////////////////////////////////////////
*/
	for(;;)
	{
		osDelay(1);
		/* Delete the Init Thread */
		if( osThreadTerminate(defaultTaskHandle) != osOK ){
			Error_Handler();
		}
	}
}


/**
  * @brief  This function will be call in Lwip in each event on netconn
  * 		Send event on xQueue_events in nonBlockingTCP
  *
  * @param  struct netconn *conn connection structure
  * @param  senum netconn_evt evt structure of the event on connection
  * @param  uint16_t length of the message in byte
  * @return None
  */
static void netCallback(struct netconn *conn, enum netconn_evt evt, uint16_t length)
{
	err_t err;
    //Show some callback information (debug)
    printf("sock:%lu\tsta:%lu\tevt:%d\tlen:%d\ttyp:%lu\tfla:%02X\terr:%d", \
            (uint32_t)conn,conn->state,evt,length,conn->type,conn->flags,conn->pending_err);

    netconn_events events ;

    //If netconn got error, it is close or deleted, dont do treatments on it.
    if (conn->pending_err)
    {
		events.nc = conn ;
		events.type = conn->pending_err;
	    xQueueSend(xQueue_events, &events, WAITING_SEND_TCPEVENT_TIME);
        return;
    }
    //Treatments only on rcv events.
    switch (evt) {
		case NETCONN_EVT_RCVPLUS:
			events.nc = conn ;
			events.type = evt ;
			break;
		default:
			return;
			break;
    }

    //Send the event to the queue
    xQueueSend(xQueue_events, &events, WAITING_SEND_TCPEVENT_TIME);
}

/**
  * @brief 	Initialize a server netconn and listen port
  *
  * @param  struct netconn **conn connection structure pointer of pointer
  * @param  uinnt16_t port of the new tcp server
  * @param  netconn_callback callback to call at each TCP event
  * @return None
  */
static void set_tcp_server_netconn(struct netconn **nc, uint16_t port, netconn_callback callback)
{
    if(nc == NULL)
    {
        printf("%s: netconn missing .\n",__FUNCTION__);
        Error_Handler();
        return;
    }
    *nc = netconn_new_with_callback(NETCONN_TCP, netCallback);
    if(!*nc) {
        printf("Status monitor: Failed to allocate netconn.\n");
        Error_Handler();
        return;
    }
    netconn_set_nonblocking(*nc,NETCONN_FLAG_NON_BLOCKING);
    //netconn_set_recvtimeout(*nc, 10);
    err_t err=netconn_bind(*nc, IP_ADDR_ANY, port);
    if(err!=ERR_OK)
    	Error_Handler();
    err=netconn_listen(*nc);
    if(err!=ERR_OK)
    	Error_Handler();
}

/**
  * @brief 	Close and delete a socket properly
  *
  * @param  struct netconn *nc connection structure pointer
  * @return None
  */
static void close_tcp_netconn(struct netconn *nc)
{
    nc->pending_err=ERR_CLSD; //It is hacky way to be sure than callback will don't do treatment on a netconn closed and deleted
    err_t err=netconn_close(nc);
    if(err!=ERR_OK)
    	Error_Handler();
    err=netconn_delete(nc);
    if(err!=ERR_OK)
    	Error_Handler();
}

/**
  * @brief 	This task manage each netconn connection without block anything
  *
  * @param  void *pvParameters launching parameters (for this task anything)
  * @return None
  */
static void nonBlockingTCP(void *pvParameters)
{

    struct netconn *nc = NULL; // To create servers
    UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    set_tcp_server_netconn(&nc, 22, netCallback);
    debug_printf(DEBUG_NET,"Server netconn %u ready on port %u.\n",(uint32_t)nc, 22);

    struct netbuf *netbuf = NULL; // To store incoming Data
    struct netconn *nc_in = NULL; // To accept incoming netconn

    char buf[50];
    char* buffer;
    uint16_t len_buf;
    err_t recv_err;
    uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    while(1) {
    	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        netconn_events events;
        xQueueReceive(xQueue_events, &events, portMAX_DELAY); // Wait here an event on netconn
        if (events.nc->state == NETCONN_LISTEN) // If netconn is a server and receive incoming event on it
        {
            printf("Client incoming on server %u.\n", (uint32_t)events.nc);
            int err = netconn_accept(events.nc, &nc_in);
            if (err != ERR_OK)
            {
                if(nc_in){
                	recv_err = netconn_delete(nc_in);
                    if(recv_err!=ERR_OK)
                    	Error_Handler();
                }
                continue;
            }
            debug_printf(DEBUG_NET, "New client is %u.\n",(uint32_t)nc_in);
            ip_addr_t client_addr; //Address port
            uint16_t client_port; //Client port
            err=netconn_peer(nc_in, &client_addr, &client_port);
            if(err!=ERR_OK)
            	Error_Handler();
            snprintf(buf, sizeof(buf), "Your address is %d.%d.%d.%d:%u.\r\n",
                    ip4_addr1(&client_addr), ip4_addr2(&client_addr),
                    ip4_addr3(&client_addr), ip4_addr4(&client_addr),
                    client_port);
            debug_printf(DEBUG_NET, buf);
            accepted_socket_t item;
			item.idx = getAvailableSocket();
			item.conn = nc_in;//events.nc;
			setAvailableSocket(item);
        }
        else if(events.nc->state != NETCONN_LISTEN && events.type == ERR_RST) // If netconn is the client and error occurs on connection
        {
            int idx_pool = getSocketPoolIdx(events.nc);
            if(idx_pool>=0) {
    			flushSocketPool(idx_pool);
    			recv_err = netconn_delete(events.nc);
    		    if(recv_err!=ERR_OK)
    		    	Error_Handler();
            }
        }
        else if(events.nc->state != NETCONN_LISTEN) // If netconn is the client and receive data
        {
			struct netbuf *inbuf;
			char* buf;
			u16_t buflen;
			recv_err = netconn_recv(events.nc, &inbuf);

			if (recv_err == ERR_OK)
			{
				if (netconn_err(events.nc) == ERR_OK)
				{
					do {
						err_t data_err;
						data_err=netbuf_data(inbuf, (void**)&buf, &buflen);
						if(data_err!=ERR_OK)
							break;
////						osStatus_t reply_send;
						BaseType_t reply_send;
						int idx_pool = getSocketPoolIdx(events.nc); //getAvailableSocket_fromNetconn(events.nc);
						for (int i=0;idx_pool>=0 && i<buflen;i++)
						{
////							reply_send = osMessageQueuePut(state_socket_pool.NetRxQueueHandle_Pool[idx_pool], &(buf[i]), 0U, 1000);
							reply_send = xQueueSend(state_socket_pool.NetRxQueueHandle_Pool[idx_pool], &(buf[i]), 1000U);
							if(reply_send!=pdTRUE)
								reply_send--;
//							HAL_UART_Transmit(&huart3, &(buf[i]), 1, HAL_MAX_DELAY);
						}
					} while(netbuf_next(inbuf) >= 0);
				}else{
	                int idx_pool = getSocketPoolIdx(events.nc);
	                if(idx_pool>=0) {
						flushSocketPool(idx_pool);
						close_tcp_netconn(events.nc);
	                }
	                printf("Error on netconn %u, close it \n",(uint32_t)events.nc);
				}
				netbuf_delete(inbuf);
			}else{
                int idx_pool = getSocketPoolIdx(events.nc);
                if(idx_pool>=0) {
					flushSocketPool(idx_pool);
					close_tcp_netconn(events.nc);
                }
                printf("Error read netconn %u, close it \n",(uint32_t)events.nc);
			}
        }
    }
}
