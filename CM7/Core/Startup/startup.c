/*
 * startup.c
 *
 *  Created on: Apr 21, 2022
 *      Author: stefano
 */


#include "main.h"
#include <string.h>


extern void bootLoader(void);
extern void main(void);
extern void __libc_init_array(void);

extern uint32_t _estack,_sdata,_edata,_sidata,_s_isr_init_data,_s_isr_end_data,_si_isr_init_data,_sbss,_ebss,_sramfunc,_eramfunc,_siramfunc;
extern int8_t error_n;

static void loadRegion(uint32_t *sRam, uint32_t *eRam, uint32_t *sRom){
	uint32_t *sdata=sRam;
	uint32_t *edata=eRam;
	uint32_t *sidata=sRom;
	while(sdata<edata){
		*sdata=*sidata;
		sdata++;
		sidata++;
	}
}
static void initRegion(uint32_t *sRam, uint32_t *eRam){
	uint32_t *sdata=sRam;
	uint32_t *edata=eRam;
	while(sdata<edata){
		*sdata=0;
		sdata++;
	}
}

void blInit(void){
	__disable_irq();//questa � una sezione critica... non voglio IRQ
	initRegion(&_sbss,&_ebss);	/* Zero fill the bss segment. */
	loadRegion(&_sdata,&_edata,&_sidata);/* Copy the data segment initializers from flash to SRAM */
	/*
	loadRegion(&_s_isr_init_data,&_s_isr_end_data,&_si_isr_init_data);// Copy the isr segment initializers from flash to SRAM
	loadRegion(&_sramfunc,&_eramfunc,&_siramfunc);// Copy the isr segment initializers from flash to SRAM
	*/

	SystemInit();
	/*
	HAL_Init();
	SystemInit();
	SCB->VTOR = D1_ITCMRAM_BASE;
	__enable_irq();
	SCB_InvalidateDCache();
	SCB_DisableDCache();
	SCB_InvalidateICache();
	SCB_DisableICache();
	MPU_Config();
	SCB_EnableICache();
	SCB_EnableDCache();
	SystemClock_Config();
	*/
	__libc_init_array();
	main();
	while(1);
}
