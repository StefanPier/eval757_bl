/*
 * IPC_common.h
 *
 *  Created on: 22 feb 2022
 *      Author: stefano
 */

#ifndef INC_IPC_COMMON_H_
#define INC_IPC_COMMON_H_
#include "command.h"
#include "steute.h"
#include "manage_debug.h"

//IPC Channel types
typedef enum{
	IPC_UPDATE_CHANNEL,		//Channel Update from CM4 to CM7
	IPC_CMD_CHANNEL,		//Channel commando from CM7 to CM4 and reply
	IPC_STEUTE_CHANNEL,		//Channel used to send from CM7 to CM4 steute switch state
	IPC_DEBUG_CHANNEL,		//Channel used to send from CM4 to CM7 debug messsage
}IPC_channel_t;

//IPC Command Message role
typedef enum{
	IPC_CMD_REQUEST,
	IPC_CMD_REPLY,
}IPC_cmd_role_t;

#define ADC_CHANNELS			10
#define INPUT_CHANNELS			10
#define OUTPUT_CHANNELS			10
//Minimum refresh time [ms] for cm4state on cm7 core
#define	UPDATE_TIME_CM4_STATE	100

//IPC_UPDATE_CHANNEL IPC Channel types
typedef struct update_cm4_state_t{
	IPC_channel_t channel;					//IPC Channel id
	uint32_t adc_value[ADC_CHANNELS];
	uint8_t in_value[INPUT_CHANNELS];
	uint8_t out_value[OUTPUT_CHANNELS];
	steute_state_t steute_state;
} __attribute__((__packed__)) update_cm4_state_t;

//IPC_CMD_CHANNEL IPC Channel types
typedef struct cmd_msg_t{
	IPC_channel_t channel;					//IPC Channel id
	uint32_t id;							//Command id
	uint32_t cmd_id;						//Command name as enum
	IPC_cmd_role_t cmd_type;				//Command Type
	unsigned char cmd_data[N_MAX_DATA_LENGHT];
} __attribute__((__packed__)) cmd_msg_t;

//IPC_STEUTE_CHANNEL IPC Channel types
typedef struct steute_msg_t{
	IPC_channel_t channel;					//IPC Channel id
	steute_state_t steute_state;			//Steute state data
} __attribute__((__packed__)) steute_msg_t;

//IPC_DEBUG_CHANNEL IPC Channel types
typedef struct cm4_debug_msg_t{
	IPC_channel_t channel;					//IPC Channel id
	char msg_data[MAX_BYTE_FOR_IPC_DEBUG];
} __attribute__((__packed__)) cm4_debug_msg_t;


//Union of message sending on IPC virtqueue:
//ATTENTION NEED TO INVOLVE ALL STRUCT TYPE USED ON IPC CHANNEL !!!!
//(THIS TYPE IS USED TO CONSTRUCT QUEUE SIZE)
typedef union {
	update_cm4_state_t update_cm4_state;
	cmd_msg_t cmd_msg;
	steute_msg_t steute_msg;
	cm4_debug_msg_t cm4_msg;
}__attribute__((__packed__)) raw_state_t;

//unsigned int
#define	IPC_msg_t	raw_state_t

#endif /* INC_IPC_COMMON_H_ */
