/*
 * bootloader_settings.h
 *
 *  Created on: Apr 21, 2022
 *      Author: stefano
 */

#ifndef INC_UPDATER_SETTINGS_H_
#define INC_UPDATER_SETTINGS_H_

#include "main.h"
#include "command.h"

#define STACK_CONTROL_WORD                   0xAC
#define STACK_CONTROL_WORD_U32               0xACACACAC

#ifdef CORE_CM7
	#include "cmsis_os.h"
#endif

//Enum to establish the reboot reason if there is one
enum{
	No_Reset_Operation,
	Force_Update=0x12369874,
	Force_Start_App=0x47896321
};

typedef struct __attribute__((__packed__)) {
	uint32_t Ver1;
	uint32_t Ver2;
	uint32_t Ver3;
	uint32_t Ver4;
}app_ver_t;

typedef enum __attribute__((__packed__)){
	CM7_ID,
	CM4_ID
}id_core_t;

typedef struct __attribute__((__packed__)){
	uint32_t cmd;
	uint32_t not_cmd;
	uint32_t equal_cmd;
	uint32_t crc;
}internal_bl_cmd_t;

//!Structure to know update information
typedef struct __attribute__((__packed__, aligned (32))) {
    void (*appStartCM7)(void);		//Start Function for CM7 Core (not used on CM4 FW)
    void (*appStartCM4)(void);		//Start Function for CM4 Core (not used on CM7 FW)
    uint32_t CodeAddressCM7;		//Start Program Address for CM7
    uint32_t CodeAddressCM4;		//Start Program Address for CM4
    uint32_t id_core;
    app_ver_t app_ver;
    uint32_t lengthCM7;
    uint32_t lengthCM4;
    uint8_t app_sha256[32];
    uint32_t dummy[4];				//To allign structure to 32
    uint32_t crc_struct;
}app_info_t;

//!Info Structure on bootloader
typedef struct __attribute__((__packed__)) {
	uint32_t Ver1;
	uint32_t Ver2;
	uint32_t Ver3;
	uint8_t boot_sha256[32];
}bootloader_ver_t;

//!Info Structure to erase flash
typedef struct __attribute__((__packed__)) {
	uint32_t bank;
	uint32_t bank_c;
	uint32_t sector;
	uint32_t sector_c;
	uint32_t nsector;
	uint32_t nsector_c;
}update_erase_t;

//!Info Structure to erase all flash
typedef struct __attribute__((__packed__)) {
	uint32_t start_address;
	uint32_t start_address_c;
	uint32_t stop_address;
	uint32_t stop_address_c;
}update_erase_all_t;

//!Info Structure to write flash
typedef struct __attribute__((__packed__)) {
	uint32_t waddress;
	uint32_t wsize;
	uint8_t wdata[N_MAX_DATA_LENGHT-2*sizeof(uint32_t)];
}update_write_t;

//!Info Structure to read flash
typedef struct __attribute__((__packed__)) {
	uint32_t raddress;
	uint32_t rsize;
	uint8_t rdata[N_MAX_DATA_LENGHT-2*sizeof(uint32_t)];
}update_read_t;

#endif /* INC_UPDATER_SETTINGS_H_ */
