/*
 * binaryprotocol.h
 *
 *	Header file del
 *
 * Created: 28/02/2022 11:01:35
 *  Author: Stefano
 */ 
#ifndef BINARYPROTOCOL_H_
#define BINARYPROTOCOL_H_

#include "command.h"

/*
 *
//#define UART_CHECK           0x7F
//#define GET_CMD              0x00             /* Get command              */
//#define VER_CMD              0x01             /* Get version ID and Read Protection Status	*/
//#define GID_CMD              0x02             /* Get device ID            */
//#define RM_CMD               0x11             /* Read memory command      */
//#define GO_CMD               0x21             /* Go command               */
//#define WM_CMD               0x31             /* Write memory command     */
//#define ER_CMD               0x44             /* Extended Erase command   */
//#define WR_PRO_CMD           0x63             /* Enables the write protection for some sectors command   */
//#define WR_UNP_CMD           0x73             /* Disables the write protection for all Flash memory sectors command   */
//#define RD_PRO_CMD           0x82             /* Enables the read protection command   */
//#define RD_UNP_CMD           0x92             /* Disables the read protection command   */

/** @name Protocol command enumerator
 */
///@{
enum lista_comandi {
	FWVERSION_CMD = 0,				//0
	// Upgrade command
	ERASE_CMD,
	ERASEALL_CMD,
	WRITE_CMD,
	READ_CMD,
	CHECK_CMD,
	GO_CMD,
	WRITE_PROTECTION,
	WRITE_UNPROTECTION,
	READ_PROTECTION,
	READ_UNPROTECTION,
	LAST_BIN_CMD=98,			// Primo comando disponibile
};
///@}

//!< Initialize the protocol callback list
void binaryproto_function_init (void);

#define 	MAX_PARAMS_NUM	(N_MAX_DATA_LENGHT/sizeof(uint32_t))-1)

//Structure of data for each command
typedef struct {
	uint32_t num_params;
	uint32_t parameters[(MAX_PARAMS_NUM];
} generic_cmd_t;

#endif /* BINARYPROTOCOL_H_ */
