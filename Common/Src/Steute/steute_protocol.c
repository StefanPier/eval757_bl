#include "settings.h"

#ifdef	STEUTE_ON_CONTROL
#include "main.h"
#include "steute.h"
#include "timer.h"
#ifdef STEUTE_CONTROL_ON_CM7
	#include "cmsis_os.h"
	#include "manage_debug.h"
	#include "IPC_m7.h"
#endif
//External steute state used on steute.c
extern steute_state_t steute_state;

//Used for CRC16 calculation
const uint16_t crc_table[16] = {
     0x0000,
     0x1021,
     0x2042,
     0x3063,
     0x4084,
     0x50a5,
     0x60c6,
     0x70e7,
     0x8108,
     0x9129,
     0xa14a,
     0xb16b,
     0xc18c,
     0xd1ad,
     0xe1ce,
     0xf1ef
};

#define CRC16_INIT_VALUE	0x0000

/** @brief CRC16 CCITT, polynomial 0x1021, Init 0x0000
 *
 *  @param	cBuffer * cbuffer_to_crc buffer to verify crc
 *  @param  uint32_t size: buffer dimension
 *  @return crc (uint16_t)
 */
uint16_t CRC16_8(cBuffer * cbuffer_to_crc, uint32_t size)
{
//-------------------------------------------------------------------------------------------
//       CRC a 16 bit che opera su dati a 8 bit (CCITT/ITU).
//       "size" indica il numero dei dati a 8 bit su cui viene calcolato il crc
//-------------------------------------------------------------------------------------------

   uint32_t index_on_cbuff;
   uint16_t crc;
   for (index_on_cbuff=0,crc=CRC16_INIT_VALUE;index_on_cbuff<size;index_on_cbuff++) {
          crc = ((crc << 4) | (((uint16_t)bufferGetAtIndex(cbuffer_to_crc, index_on_cbuff)) >> 4)) ^ crc_table[crc >> 12];
          crc = ((crc << 4) | (((uint16_t)bufferGetAtIndex(cbuffer_to_crc, index_on_cbuff) & 0x0f))) ^ crc_table[crc >> 12];
   }
   //Augment message with 16 zero bits
   crc = (crc << 4) ^ crc_table[crc >> 12];
   crc = (crc << 4) ^ crc_table[crc >> 12];
   crc = (crc << 4) ^ crc_table[crc >> 12];
   crc = (crc << 4) ^ crc_table[crc >> 12];

   return crc;
}

/** @brief Function that finds a true steute packed and copies it on a buffer
 *
 *  @param	steute_status_complete_t *status_packet pointer to status/pair message from steute process
 *  @return FALSE_STEUTE_PACKET if no stute packet present
 *  		TRUE_STEUTE_PACKET otherwise
 */
int process_steute_packet (cBuffer * buffer_to_analyze, uint8_t *stxetxRxPacket){

	int foundpacket = FALSE_STEUTE_PACKET;
	int i;
	uint16_t length;
	uint16_t checksum_calculated, checksum_read;
	static bool first_entry = true;
	static uint32_t tout = 0;
	//unsigned char type;


	// process the buffer
	// go through buffer looking for packets
	// the STX must be located at least STXETX_HEADERLENGTH+STXETX_TRAILERLENGTH from end
	// otherwise we must not have a complete packet
	while( buffer_to_analyze->datalength >= ((unsigned int)STEUTE_HEADERLENGTH+(unsigned int)STEUTE_TRAILERLENGTH) )
	{
		// qui la prima volta che entro devo inizializzare il timeout
		if(first_entry)
		{
			tout = timer_TimerSet(STEUTE_TOUT);
			first_entry = false;
		}
		else if(timer_TimerTest(tout))
		{
			// dump receive buffer contents to relieve deadlock
			bufferFlush(buffer_to_analyze);
			first_entry = true;
			break;
		}

		// look for a potential start of packet
		if(bufferGetAtIndex(buffer_to_analyze, STEUTE_START_POS) == STEUTE_START_BYTE)
		{
			// if this is a start, then get the length
			length = bufferGetAtIndex(buffer_to_analyze, STEUTE_LENGHT_POS);

			// now we must have at least STEUTE_HEADERLENGTH+length+STEUTE_TRAILERLENGTH in buffer to continue
			if(buffer_to_analyze->datalength >= ((unsigned int)length+(unsigned int)STEUTE_ETX_LEN))
			{
				// check to see if ETX is in the right position
				if ((bufferGetAtIndex(buffer_to_analyze, length) == STEUTE_CR_BYTE) &&
					(bufferGetAtIndex(buffer_to_analyze, length+1) == STEUTE_LF_BYTE)	)
				{
					// found potential packet
					// test  CRC16 CCITT, polynomial 0x1021, Init 0x0000
					checksum_calculated = CRC16_8(buffer_to_analyze, ((uint32_t)length-STEUTE_CRC_LEN));
					checksum_read = (((uint16_t)bufferGetAtIndex(buffer_to_analyze, length-STEUTE_CRC_LEN)<<8)+((uint16_t)bufferGetAtIndex(buffer_to_analyze, length-STEUTE_CRC_LEN + 1)));
					// compare checksums
					if(checksum_calculated == checksum_read)
					{
						//we have a packet!
						foundpacket = TRUE_STEUTE_PACKET;

						// copy data to buffer
						// (don't copy STX, ETX, or CHECKSUM)
						for(i = 0; i < length+STEUTE_ETX_LEN; i++)
						{
							stxetxRxPacket[i] = bufferGetAtIndex(buffer_to_analyze, i);
						}

						// dump this packet from the
						bufferDumpFromFront(buffer_to_analyze, length+STEUTE_ETX_LEN);
						first_entry = true;

						// done with this processing session
						break;
					}
					else
					{
						// for now, we dump these
						// dump this STX
// 						bufferGetFromFront(rxBuffer);
						bufferDumpFromFront(buffer_to_analyze, length+STEUTE_ETX_LEN);
						first_entry = true;
#ifdef STEUTE_CONTROL_ON_CM7
						debug_printf(DEBUG_STEUTE,"Steute CRC BAD\n\r");
#endif

					}
				}
				else
				{
					// no ETX or ETX in wrong position
					// dump this STX
					uint8_t dummy;
					bufferGetFromFront(buffer_to_analyze, &dummy);
					first_entry = true;
#ifdef STEUTE_CONTROL_ON_CM7
					debug_printf(DEBUG_STEUTE,"Steute wrongpos\n\r");
#endif
				}
			}
			else
			{
				// not enough data in buffer to decode pending packet
				// wait until next time
// 				UARTprintf("not enough data in buffer to decode pending packet\r\n");
				break;
			}
		}
		else
		{
			// this is not a start, dump it
			uint8_t dummy;
			bufferGetFromFront(buffer_to_analyze, &dummy);
			first_entry = true;
//			debug_printf(DEBUG_STEUTE,"Steute NotStart\n\r");
		}
	}

	 //check if receive buffer is full with no packets decoding
	 //(ie. deadlocked on garbage data or packet that exceeds buffer size)
	if(!bufferIsNotFull(buffer_to_analyze))
	{
		// dump receive buffer contents to relieve deadlock
		bufferFlush(buffer_to_analyze);
		first_entry = true;
	}
		

	return foundpacket;
}

/** @brief Perform action based on status and pair action from steute
 *
 *  @param	steute_status_complete_t *status_packet pointer to status/pair message from steute process
 *  @return int as RES_SUCCESS
 */
static int	analyze_steute_status_pair(steute_status_complete_t *  status_packet){
	switch(status_packet->status.status_code){
	case STEUTE_RX_STARTED:
		steute_state.started=STEUTE_STARTED;
		break;
	case STEUTE_CMD_RX:
	case STEUTE_CMD_IMP:
	case STEUTE_CMD_UNK:
	case STEUTE_CRC_ERR:
	case STEUTE_TOO_LONG:
	case STEUTE_TOO_SMALL:
	case STEUTE_NOT_ENOUGHT:
	case STEUTE_INV_PAIR:
	case STEUTE_SYS_ERR:
		steute_state.status_code = status_packet->status.status_code;
		//TODO Action over cmd_send:
		/*
		 switch(steute_state.cmd_send){
			 case :
				 do something
		 }
		 steute_state.cmd_send
		 */
		steute_state.status_code = status_packet->status.status_code;
		break;
	}
	for(int i=0;i<4;i++)
		steute_state.status_detail[i] = status_packet->status.status_detail[i];
	return RES_SUCCESS;
}

/** @brief Check on gpio_input masked as input_mask, if gpio_state change
 *
 *  @param	uint8_t gpio_input: binary input from pins
 *  @param	uint8_t input_mask: mask to indivisually select state change
 *  @param	uint8_t *gpio_state: previous selected input change ( STEUTE_UNPRESSED, STEUTE_PRESSED)
 *  @return int as RES_SUCCESS if ok
 *  		inst as RES_FAIL otherwise
 */
static int switch_update(uint8_t gpio_input,uint8_t input_mask, uint8_t *gpio_state){
	int update=RES_FAIL;
	if(gpio_input & input_mask){
		if(*gpio_state!=STEUTE_UNPRESSED){
			*gpio_state = STEUTE_UNPRESSED;
			update = RES_SUCCESS;
		}
	}else{
		if(*gpio_state!=STEUTE_PRESSED){
			*gpio_state = STEUTE_PRESSED;
			update = RES_SUCCESS;
		}
	}
	return update;
}

/** @brief Check on gpio_input masked as input_mask, if gpio_state change
 *
 *  @param	steute_switch_struct_t *  switch_packet: structure for steute gpio input
 *  @return int as RES_SUCCESS if at least one change
 *  		inst as RES_FAIL otherwise
 */
static int analyze_switch_data(steute_switch_struct_t *  switch_packet){
	int update = RES_FAIL;
	steute_state.packet_counter = switch_packet->packet_counter;
	steute_state.rssi = switch_packet->data_detail.rssi_level;
	//Read Switch:
	update|=switch_update(switch_packet->data_detail.gpio_input, STEUTE_LEFT_PEDAL_MASK, &steute_state.left_pedal);
	update|=switch_update(switch_packet->data_detail.gpio_input, STEUTE_CENTRAL_PEDAL_MASK, &steute_state.central_pedal);
	update|=switch_update(switch_packet->data_detail.gpio_input, STEUTE_RIGHT_PEDAL_MASK, &steute_state.right_pedal);
	update|=switch_update(switch_packet->data_detail.gpio_input, STEUTE_LEFT_UPPER_PEDAL_MASK, &steute_state.left_upper_pedal);
	update|=switch_update(switch_packet->data_detail.gpio_input, STEUTE_RIGHT_UPPER_PEDAL_MASK, &steute_state.right_upper_pedal);

	if(update  == RES_SUCCESS){
		//Header command on IPC
		IPC_msg_t steute_state_msg;
		steute_state_msg.steute_msg.channel = IPC_STEUTE_CHANNEL;
		steute_state_msg.steute_msg.steute_state = steute_state;
		debug_printf(DEBUG_STEUTE, "left_pedal=%d\ncentral_pedal=%d\nright_pedal=%d\nleft_upper_pedal=%d\nright_upper_pedal=%d\n\r",
				steute_state.left_pedal,steute_state.central_pedal ,steute_state.right_pedal,steute_state.left_upper_pedal,steute_state.right_upper_pedal);
#ifdef	STEUTE_CONTROL_ON_CM7
		//Tx Event on IPC command queue
		xQueueSend(ipc_queue.QueueHandle,&steute_state_msg,0);
#endif
	}
	return update;
}

/** @brief Check steute packet and perform action depending of they
 *
 *  @param	uint8_t * packet poiter (has to be converted to steute_rxheader_struct_t*)
 *  @return none
 */
void Analyze_steute_packet (uint8_t * packet){
	steute_rxheader_struct_t *header=(steute_rxheader_struct_t *)packet;
	switch(header->id){
	case STEUTE_STATUS_AND_PAIR_ID:
		analyze_steute_status_pair((steute_status_complete_t*)packet);
		break;
	case STEUTE_STEUTEACTIVE_SLEEP_ID:
		break;
	case STEUTE_SWITCHING_ID:
		analyze_switch_data((steute_switch_struct_t*)packet);
		break;
	case STEUTE_CONFMODE_DATA_ID:
	case STEUTE_CONFMODE_STATUS_ID:
	case STEUTE_CONFMODE_ACTIVATION_ID:
	case STEUTE_CONFMODE_ID:
	default:
		break;
	}
}

#endif

