/*
 * manage_debug.c
 *
 *  Created on: 26 gen 2022
 *      Author: stefano
 */


#include <manage_debug.h>
#include "main.h"
#include "stdio.h"
#include "stdarg.h"
#include "string.h"
#include "buffer.h"


char cmd_printf_enable[LAST_DBG] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};		//Default for customer


#ifdef	CORE_CM7
	#include "cmsis_os.h"
	#include "lwip.h"
	osThreadId DebugTaskHandle;

	//Communication striuctures
	#define	SERIAL_DEBUG	huart1
	extern UART_HandleTypeDef SERIAL_DEBUG;
	debug_static_buffer_t debug_queue;

#endif


/*
 * Make Rx OS queue for to debug purpose
 */
void MakeDebugQueue(){
#ifdef	CORE_CM7
    /* Create a queue capable of containing 10 uint64_t values. */
	debug_queue.QueueHandle = xQueueCreateStatic( MAX_BYTE_FOR_DEBUG_QUEUE,
													sizeof(uint8_t),
													debug_queue.QueueBuffer,
													&debug_queue.QueueControlBlock );
	if(debug_queue.QueueHandle==NULL){
		Error_Handler();
	}
#endif
}

void debug_printf(bool debug, const char *s, ...){
	char tmp[MAX_BYTE_FOR_DEBUG_QUEUE];
	#ifdef	CORE_CM7
	/*
	if(debug<NOT_USED_DEBUG){
		tmp[0]=0;
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		va_list ap;
		va_start(ap, s);
		//vsprintf(tmp,s,ap);
		vsnprintf(tmp,MAX_BYTE_FOR_DEBUG_QUEUE,s,ap);
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		// metto nel buffer
		for(uint32_t i=0; i<strlen(tmp);i++)
		{
			BaseType_t reply_send = xQueueSend(debug_queue.QueueHandle, ( void * ) &tmp[i], 0);
//			if( reply_send != pdPASS )
//			{
//				Error_Handler();
//			}
		}
		va_end(ap);
	}
	*/
	if(debug){
		tmp[0]=0;
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		va_list ap;
		va_start(ap, s);
		//vsprintf(tmp,s,ap);
		vsnprintf(tmp,MAX_BYTE_FOR_DEBUG_QUEUE,s,ap);
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		// metto nel buffer
		for(uint32_t i=0; i<strlen(tmp);i++)
		{
			BaseType_t reply_send = xQueueSend(debug_queue.QueueHandle, ( void * ) &tmp[i], 0);
//			if( reply_send != pdPASS )
//			{
//				Error_Handler();
//			}
		}
		va_end(ap);
	}
#endif
}


//Flush out from serial debug
void DebugTask(void const * argument){
#ifdef	CORE_CM7
	uint8_t msd_data_ptr[MINIMUM_DEBUG_BYTE];
	BaseType_t res;

	while(1){
		//msg_count = uxQueueMessagesWaiting(steute_rx_queue.QueueHandle);
		res =  xQueueReceive(debug_queue.QueueHandle,msd_data_ptr,portMAX_DELAY);
		if(res == pdTRUE ){
			HAL_UART_Transmit(&SERIAL_DEBUG, msd_data_ptr, sizeof(msd_data_ptr), HAL_MAX_DELAY);
		}
		osDelay(1);
	};
#endif
}

